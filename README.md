# Create a trading bot

In this workshop you will create a trading bot in Node.js.

### 1) Install Node.js

* if you are on Ubuntu:
```bash
curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -

sudo apt-get install -y nodejs
```

* Otherwise: https://nodejs.org/en/download/

### 2) Create a demonstration account
You need to create account on FXCM, this platform is what we call a "broker".
We will use it as an API for communicating with the FOREX marketplace.

link: https://www.fxcm.com/uk/forex-trading-demo/

### 3) Generate a token API

Go to the link below, and connect to your newly created account.
When connected, go to your profil settings and click on "Tokens management".

link: https://tradingstation.fxcm.com/?lc=fr_FR

### 4) Connect to REST API via socket

With your token, you will connect to FXCM via their REST API.

Tips: take a look to the [socket.io-client](https://www.npmjs.com/package/socket.io-client) npm package.

link: https://github.com/fxcm/RestAPI

#### 4.1) Subscribe to pair live feed

Now that you've got an open connection via socket, you need to subscribe to a live feed for getting updated of market price.

### 5) Implement your first technical indicator

example: Floor Pivot Point

### 6) Create your virtual wallet

For testing purpose, you don't want to really register trading action so this will be usefull.
